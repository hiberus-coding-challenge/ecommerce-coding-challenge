package com.hiberus.bill.entrypoints.reactive.web.controllers;

import com.hiberus.bill.usecase.BillingUseCase;
import com.hiberus.ecommerce.model.entities.BillingSummary;
import com.hiberus.ecommerce.model.entities.Order;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class BillController {

    private final BillingUseCase billingUseCase;

    @PostMapping("/billing-summary")
    @ApiResponses(
            @ApiResponse(code = 200, message = "Success \uD83D\uDE42", response = BillingSummary.class)
    )
    public Mono<BillingSummary> getSumOfProducts(@RequestBody Order order) {
        return Mono.just(billingUseCase.getBillingSummary(order.getProducts()));
    }
}
