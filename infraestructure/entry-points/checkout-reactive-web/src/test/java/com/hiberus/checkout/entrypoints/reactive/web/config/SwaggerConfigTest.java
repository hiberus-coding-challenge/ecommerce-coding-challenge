package com.hiberus.checkout.entrypoints.reactive.web.config;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import springfox.documentation.spring.web.plugins.Docket;

import static org.assertj.core.api.Assertions.assertThat;

public class SwaggerConfigTest {

    private SwaggerConfig swaggerConfig;

    @Before
    public void setUp(){
        this.swaggerConfig = new SwaggerConfig();
    }

    @Test
    @DisplayName("Should return a Docket instance")
    public void test_api() {
        assertThat(swaggerConfig.api())
                .isInstanceOf(Docket.class);
    }
}
