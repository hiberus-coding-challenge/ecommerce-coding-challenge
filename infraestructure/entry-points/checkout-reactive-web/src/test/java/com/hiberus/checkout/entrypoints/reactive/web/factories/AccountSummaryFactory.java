package com.hiberus.checkout.entrypoints.reactive.web.factories;

import com.hiberus.checkout.entrypoints.reactive.web.dtos.AccountSummary;
import com.hiberus.ecommerce.model.entities.BillingSummary;
import com.hiberus.ecommerce.model.entities.Order;

public final class AccountSummaryFactory {

    public static AccountSummary accountSummaryBuilder(BillingSummary billingSummary, Order logistic) {
        return AccountSummary.builder()
                .orderId(logistic.getId())
                .date(logistic.getDate())
                .clientId(logistic.getClientId())
                .direction(logistic.getDirection())
                .products(logistic.getProducts())
                .quantityProducts(billingSummary.getQuantity())
                .total(billingSummary.getTotal())
                .build();
    }
}
