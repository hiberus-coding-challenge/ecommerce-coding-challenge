package com.hiberus.checkout.entrypoints.reactive.web.controllers;

import com.hiberus.checkout.entrypoints.reactive.web.dtos.AccountSummary;
import com.hiberus.checkout.usecase.CreateOrderUseCase;
import com.hiberus.checkout.usecase.MakeBillingUseCase;
import com.hiberus.ecommerce.model.entities.BillingSummary;
import com.hiberus.ecommerce.model.entities.Order;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import reactor.test.StepVerifier;

import java.util.UUID;

import static com.hiberus.checkout.entrypoints.reactive.web.factories.AccountSummaryFactory.accountSummaryBuilder;
import static com.hiberus.checkout.entrypoints.reactive.web.factories.BillingSummaryFactory.billingSummaryBuilder;
import static com.hiberus.checkout.entrypoints.reactive.web.factories.OrderFactory.orderBuilder;
import static org.mockito.Mockito.when;
import static reactor.core.publisher.Mono.just;

@RunWith(MockitoJUnitRunner.class)
public class CheckoutControllerTest {

    @InjectMocks
    private CheckoutController checkoutController;

    @Mock
    private MakeBillingUseCase makeBillingUseCase;

    @Mock
    private CreateOrderUseCase createOrderUseCase;

    @Test
    public void should_trigger_tow_services_Bill_and_logistic() {
        Order incomingOrder = orderBuilder().build();
        Order createdOrder = orderBuilder()
                .id(UUID.randomUUID().toString())
                .build();

        BillingSummary billingSummary = billingSummaryBuilder(incomingOrder)
                .build();

        when(makeBillingUseCase.execute(ArgumentMatchers.any(Order.class)))
                .thenReturn(just(billingSummary));

        when(createOrderUseCase.execute(ArgumentMatchers.any(Order.class)))
                .thenReturn(just(createdOrder));

        AccountSummary accountSummary = accountSummaryBuilder(billingSummary, createdOrder);

        StepVerifier.create(checkoutController.checkout(incomingOrder))
                .expectNext(accountSummary)
                .verifyComplete();

        Mockito.verify(makeBillingUseCase, Mockito.times(1))
                .execute(ArgumentMatchers.any(Order.class));

        Mockito.verify(createOrderUseCase, Mockito.times(1))
                .execute(ArgumentMatchers.any(Order.class));

    }
}
