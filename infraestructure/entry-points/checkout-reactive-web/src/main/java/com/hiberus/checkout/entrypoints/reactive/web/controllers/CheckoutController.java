package com.hiberus.checkout.entrypoints.reactive.web.controllers;

import com.hiberus.checkout.entrypoints.reactive.web.dtos.AccountSummary;
import com.hiberus.checkout.usecase.CreateOrderUseCase;
import com.hiberus.checkout.usecase.MakeBillingUseCase;
import com.hiberus.ecommerce.model.entities.BillingSummary;
import com.hiberus.ecommerce.model.entities.Order;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import static reactor.core.publisher.Mono.zip;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class CheckoutController {
    private final MakeBillingUseCase makeBillingUseCase;
    private final CreateOrderUseCase createOrderUseCase;

    @PostMapping("/checkout")
    @ApiResponses(
            @ApiResponse(code = 200, message = "Success \uD83D\uDE42", response = AccountSummary.class)
    )
    public Mono<AccountSummary> checkout(@RequestBody Order order) {
        return zip(makeBillingUseCase.execute(order), createOrderUseCase.execute(order))
                .map(result -> accountSummaryMap(result.getT1(), result.getT2()));
    }

    private AccountSummary accountSummaryMap(BillingSummary billingSummary, Order logistic) {
        return AccountSummary.builder()
                .orderId(logistic.getId())
                .date(logistic.getDate())
                .clientId(logistic.getClientId())
                .direction(logistic.getDirection())
                .products(logistic.getProducts())
                .quantityProducts(billingSummary.getQuantity())
                .total(billingSummary.getTotal())
                .build();
    }
}
