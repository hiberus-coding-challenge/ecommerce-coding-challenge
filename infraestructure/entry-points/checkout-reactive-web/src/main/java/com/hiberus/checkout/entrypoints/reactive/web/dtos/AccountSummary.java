package com.hiberus.checkout.entrypoints.reactive.web.dtos;

import com.hiberus.ecommerce.model.entities.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountSummary {
    private String orderId;
    private Date date;
    private Integer clientId;
    private String direction;
    private List<Product> products;
    private Integer quantityProducts;
    private Double total;
}
