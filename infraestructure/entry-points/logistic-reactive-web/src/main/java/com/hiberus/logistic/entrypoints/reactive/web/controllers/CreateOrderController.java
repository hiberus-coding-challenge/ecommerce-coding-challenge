package com.hiberus.logistic.entrypoints.reactive.web.controllers;

import com.hiberus.ecommerce.model.entities.Order;
import com.hiberus.logistic.usecase.CreateOrderUseCase;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class CreateOrderController {

    private final CreateOrderUseCase createOrderUseCase;

    @PostMapping("/create-order")
    @ApiResponses(
            @ApiResponse(code = 200, message = "Success \uD83D\uDE42", response = Order.class)
    )
    public Mono<Order> createOrder(@RequestBody Order order) {
        return Mono.just(createOrderUseCase.execute(order));
    }
}
