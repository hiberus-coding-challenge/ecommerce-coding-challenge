package com.hiberus.checkout.repository.external;

import com.hiberus.checkout.model.exceptions.MyCustomServerException;
import com.hiberus.ecommerce.model.entities.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import static reactor.core.publisher.Mono.just;

@Repository
@RequiredArgsConstructor
public class LogisticRepository {

    private final WebClient logisticWebClient;

    public Mono<Order> createOrder(Order order) {
        return logisticWebClient.post()
                .uri(uriBuilder -> uriBuilder.path("/create-order").build())
                .body(just(order), Order.class)
                .retrieve()
                .onStatus(HttpStatus::is5xxServerError, clientResponse ->
                        Mono.error(MyCustomServerException.Type.INTERNAL_SERVER_ERROR_TRYING_TO_CREATE_THE_ORDER.build())
                )
                .bodyToMono(Order.class);
    }
}
