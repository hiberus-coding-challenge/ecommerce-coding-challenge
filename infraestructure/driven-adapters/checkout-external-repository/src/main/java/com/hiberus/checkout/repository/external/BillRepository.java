package com.hiberus.checkout.repository.external;

import com.hiberus.checkout.model.exceptions.MyCustomServerException;
import com.hiberus.ecommerce.model.entities.BillingSummary;
import com.hiberus.ecommerce.model.entities.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import static reactor.core.publisher.Mono.just;

@Repository
@RequiredArgsConstructor
public class BillRepository {

    private final WebClient billWebClient;

    public Mono<BillingSummary> makeBilling(Order order) {
        return billWebClient.post()
                .uri(uriBuilder -> uriBuilder.path("/billing-summary").build())
                .body(just(order), Order.class)
                .retrieve()
                .onStatus(HttpStatus::is5xxServerError, clientResponse ->
                        Mono.error(MyCustomServerException.Type.INTERNAL_SERVER_ERROR_TRYING_TO_CREATE_THE_ORDER.build())
                )
                .bodyToMono(BillingSummary.class);
    }
}
