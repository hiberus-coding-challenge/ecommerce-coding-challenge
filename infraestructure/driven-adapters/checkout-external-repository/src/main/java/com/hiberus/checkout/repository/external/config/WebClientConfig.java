package com.hiberus.checkout.repository.external.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientConfig {

    @Value("${checkout-app.external-services.bill-service-baseUrl}")
    private String billBaseUrl;

    @Value("${checkout-app.external-services.logistic-service-baseUrl}")
    private String logisticBaseUrl;

    @Bean
    public WebClient billWebClient() {
        return WebClient.create(billBaseUrl);
    }

    @Bean
    public WebClient logisticWebClient() {
        return WebClient.create(logisticBaseUrl);
    }
}
