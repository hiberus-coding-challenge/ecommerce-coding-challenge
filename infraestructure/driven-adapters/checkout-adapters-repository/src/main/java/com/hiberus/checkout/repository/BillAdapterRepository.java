package com.hiberus.checkout.repository;

import com.hiberus.checkout.model.gateways.BillingGateway;
import com.hiberus.checkout.repository.external.BillRepository;
import com.hiberus.ecommerce.model.entities.BillingSummary;
import com.hiberus.ecommerce.model.entities.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
@RequiredArgsConstructor
public class BillAdapterRepository implements BillingGateway {

    private final BillRepository billRepository;

    @Override
    public Mono<BillingSummary> makeBilling(Order order) {
        return billRepository.makeBilling(order);
    }
}
