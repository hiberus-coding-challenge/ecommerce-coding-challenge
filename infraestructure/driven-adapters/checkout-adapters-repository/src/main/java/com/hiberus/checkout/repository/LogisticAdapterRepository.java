package com.hiberus.checkout.repository;

import com.hiberus.checkout.model.gateways.LogisticGateway;
import com.hiberus.checkout.repository.external.LogisticRepository;
import com.hiberus.ecommerce.model.entities.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
@RequiredArgsConstructor
public class LogisticAdapterRepository implements LogisticGateway {
    private final LogisticRepository logisticRepository;

    @Override
    public Mono<Order> createOrder(Order order) {
        return logisticRepository.createOrder(order);
    }
}
