package com.hiberus.checkout.usecase.factories;

import com.hiberus.ecommerce.model.entities.Order;
import com.hiberus.ecommerce.model.entities.Product;

import java.util.Calendar;
import java.util.Collections;

public final class OrderFactory {

    public static Order.OrderBuilder orderBuilder() {
        return Order.builder()
                .clientId(1214739442)
                .date(Calendar.getInstance().getTime())
                .direction("Cr 96F 29 #12")
                .products(Collections.singletonList(Product.builder()
                        .id(1)
                        .quantity(3)
                        .cost(29.0)
                        .build())
                );
    }
}
