package com.hiberus.checkout.usecase;

import com.hiberus.checkout.model.gateways.BillingGateway;
import com.hiberus.ecommerce.model.entities.BillingSummary;
import com.hiberus.ecommerce.model.entities.Order;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import reactor.test.StepVerifier;

import static com.hiberus.checkout.usecase.factories.BillingSummaryFactory.billingSummaryBuilder;
import static com.hiberus.checkout.usecase.factories.OrderFactory.orderBuilder;
import static org.mockito.Mockito.when;
import static reactor.core.publisher.Mono.just;

@RunWith(MockitoJUnitRunner.class)
public class MakeBillingUseCaseTest {

    @InjectMocks
    private MakeBillingUseCase makeBillingUseCase;

    @Mock
    private BillingGateway billingGateway;

    @Test
    @DisplayName("Should make billing calling Billing gateway")
    public void should_make_billing() {
        Order incomingOrder = orderBuilder()
                .build();

        BillingSummary billingSummary = billingSummaryBuilder(incomingOrder)
                .build();

        when(billingGateway.makeBilling(ArgumentMatchers.any(Order.class)))
                .thenReturn(just(billingSummary));

        StepVerifier.create(makeBillingUseCase.execute(incomingOrder))
                .assertNext(res -> {
                    Assertions.assertThat(res.getQuantity()).isEqualTo(3);
                    Assertions.assertThat(res.getTotal()).isEqualTo(87.0);
                })
                .verifyComplete();
    }
}
