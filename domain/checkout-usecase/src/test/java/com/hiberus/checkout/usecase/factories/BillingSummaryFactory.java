package com.hiberus.checkout.usecase.factories;


import com.hiberus.ecommerce.model.entities.BillingSummary;
import com.hiberus.ecommerce.model.entities.Order;
import com.hiberus.ecommerce.model.entities.Product;

public final class BillingSummaryFactory {

    public static BillingSummary.BillingSummaryBuilder billingSummaryBuilder(Order order) {
        return BillingSummary.builder()
                .quantity(order.getProducts().stream().map(Product::getQuantity)
                        .reduce(Integer::sum).orElse(0))
                .total(order.getProducts().stream().map(product -> product.getCost() * product.getQuantity())
                        .reduce(Double::sum).orElse(0.0));
    }
}
