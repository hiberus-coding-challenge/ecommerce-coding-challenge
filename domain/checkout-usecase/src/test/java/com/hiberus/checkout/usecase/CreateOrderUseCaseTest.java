package com.hiberus.checkout.usecase;

import com.hiberus.checkout.model.gateways.LogisticGateway;
import com.hiberus.ecommerce.model.entities.Order;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import reactor.test.StepVerifier;

import java.util.UUID;

import static com.hiberus.checkout.usecase.factories.OrderFactory.orderBuilder;
import static org.mockito.Mockito.when;
import static reactor.core.publisher.Mono.just;

@RunWith(MockitoJUnitRunner.class)
public class CreateOrderUseCaseTest {

    @InjectMocks
    private CreateOrderUseCase createOrderUseCase;

    @Mock
    private LogisticGateway logisticGateway;

    @Test
    @DisplayName("Should create order calling Logistic gateway")
    public void should_create_order() {
        Order incomingOrder = orderBuilder()
                .build();

        Order createdOrder = orderBuilder()
                .id(UUID.randomUUID().toString())
                .build();

        when(logisticGateway.createOrder(ArgumentMatchers.any(Order.class)))
                .thenReturn(just(createdOrder));

        StepVerifier.create(createOrderUseCase.execute(incomingOrder))
                .expectNext(createdOrder)
                .verifyComplete();
    }
}
