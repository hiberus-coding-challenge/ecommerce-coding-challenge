package com.hiberus.checkout.usecase;

import com.hiberus.checkout.model.gateways.BillingGateway;
import com.hiberus.ecommerce.model.entities.BillingSummary;
import com.hiberus.ecommerce.model.entities.Order;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class MakeBillingUseCase {
    private final BillingGateway billingGateway;

    public Mono<BillingSummary> execute(Order order) {
        return billingGateway.makeBilling(order);
    }
}
