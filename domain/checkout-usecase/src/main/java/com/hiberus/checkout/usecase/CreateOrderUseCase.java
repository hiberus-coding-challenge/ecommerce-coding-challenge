package com.hiberus.checkout.usecase;

import com.hiberus.checkout.model.gateways.LogisticGateway;
import com.hiberus.ecommerce.model.entities.Order;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class CreateOrderUseCase {
    private final LogisticGateway logisticGateway;

    public Mono<Order> execute(Order order) {
        return logisticGateway.createOrder(order);
    }
}
