package com.hiberus.checkout.model.gateways;

import com.hiberus.ecommerce.model.entities.BillingSummary;
import com.hiberus.ecommerce.model.entities.Order;
import reactor.core.publisher.Mono;

public interface BillingGateway {
     Mono<BillingSummary> makeBilling(Order order);
}
