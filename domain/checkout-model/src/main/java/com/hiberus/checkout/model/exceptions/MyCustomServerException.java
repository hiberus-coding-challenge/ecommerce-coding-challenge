package com.hiberus.checkout.model.exceptions;

public class MyCustomServerException extends ApplicationException {

    public enum Type {

        INTERNAL_SERVER_ERROR_TRYING_TO_CREATE_THE_ORDER("An error occurred trying to create the order");

        private String message;

        Type(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public MyCustomServerException build() {
            return new MyCustomServerException(this);
        }

        public MyCustomServerException build(Throwable throwable) {
            return new MyCustomServerException(this, throwable);
        }
    }

    private final Type type;

    private MyCustomServerException(Type type) {
        super(type.getMessage());
        this.type = type;
    }

    private MyCustomServerException(Type type, Throwable throwable) {
        super(type.getMessage(), throwable);
        this.type = type;
    }

    public Type getType() {
        return type;
    }
}
