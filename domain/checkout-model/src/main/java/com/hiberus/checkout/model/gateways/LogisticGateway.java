package com.hiberus.checkout.model.gateways;

import com.hiberus.ecommerce.model.entities.Order;
import reactor.core.publisher.Mono;

public interface LogisticGateway {
    Mono<Order> createOrder(Order order);
}
