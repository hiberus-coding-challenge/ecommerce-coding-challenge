package com.hiberus.bill.usecase;

import com.hiberus.ecommerce.model.entities.BillingSummary;
import com.hiberus.ecommerce.model.entities.Product;

import java.util.List;

public class BillingUseCase {

    public BillingSummary getBillingSummary(List<Product> products) {
        return billingSummaryBuilder(getQuantityOfProducts(products), getTotal(products));
    }

    private Integer getQuantityOfProducts(List<Product> products) {
        return products.stream()
                .map(Product::getQuantity)
                .reduce(Integer::sum)
                .orElse(0);
    }

    private Double getTotal(List<Product> products) {
        return products.stream()
                .map(product -> product.getCost() * product.getQuantity())
                .reduce(Double::sum)
                .orElse(0.0);
    }

    private BillingSummary billingSummaryBuilder(Integer quantity, Double billTotal) {
        return BillingSummary.builder()
                .quantity(quantity)
                .total(billTotal)
                .build();
    }
}
