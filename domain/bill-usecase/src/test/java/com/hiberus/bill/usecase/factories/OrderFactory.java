package com.hiberus.bill.usecase.factories;

import com.hiberus.ecommerce.model.entities.Order;
import com.hiberus.ecommerce.model.entities.Product;

import java.util.Collections;

public final class OrderFactory {

    public static Order.OrderBuilder orderBuilder() {
        return Order.builder()
                .products(Collections.singletonList(Product.builder()
                        .id(1)
                        .quantity(3)
                        .cost(29.0)
                        .build())
                );
    }
}
