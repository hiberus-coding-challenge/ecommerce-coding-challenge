package com.hiberus.bill.usecase;

import com.hiberus.bill.usecase.factories.BillingSummaryFactory;
import com.hiberus.bill.usecase.factories.OrderFactory;
import com.hiberus.ecommerce.model.entities.BillingSummary;
import com.hiberus.ecommerce.model.entities.Order;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class BillingUseCaseTest {

    private BillingUseCase billingUseCase;

    @Before
    public void setUp() {
        this.billingUseCase = new BillingUseCase();
    }

    @Test
    public void should_get_billing_summary() {
        Order incomingOrder = OrderFactory.orderBuilder().build();

        BillingSummary billingSummary = BillingSummaryFactory.billingSummaryBuilder(incomingOrder)
                .build();

        assertThat(this.billingUseCase.getBillingSummary(incomingOrder.getProducts()))
                .isEqualTo(billingSummary);
    }
}
