package com.hiberus.logistic.usecase;

import com.hiberus.ecommerce.model.entities.Order;

import java.util.UUID;

public class CreateOrderUseCase {

    public Order execute(Order order) {
        String orderId = UUID.randomUUID().toString();

        return Order.builder()
                .id(orderId)
                .clientId(order.getClientId())
                .date(order.getDate())
                .direction(order.getDirection())
                .products(order.getProducts())
                .build();
    }
}
