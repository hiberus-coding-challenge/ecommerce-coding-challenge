package com.hiberus.checkout.config;

import com.hiberus.checkout.model.gateways.BillingGateway;
import com.hiberus.checkout.model.gateways.LogisticGateway;
import com.hiberus.checkout.usecase.CreateOrderUseCase;
import com.hiberus.checkout.usecase.MakeBillingUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseCaseConfig {

    @Bean
    public MakeBillingUseCase makeBillingUseCase(BillingGateway billingGateway) {
        return new MakeBillingUseCase(billingGateway);
    }

    @Bean
    public CreateOrderUseCase createOrderUseCase(LogisticGateway logisticGateway) {
        return new CreateOrderUseCase(logisticGateway);
    }
}
