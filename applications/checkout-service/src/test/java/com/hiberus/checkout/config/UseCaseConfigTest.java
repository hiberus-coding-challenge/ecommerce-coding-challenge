package com.hiberus.checkout.config;

import com.hiberus.checkout.model.gateways.BillingGateway;
import com.hiberus.checkout.model.gateways.LogisticGateway;
import com.hiberus.checkout.usecase.CreateOrderUseCase;
import com.hiberus.checkout.usecase.MakeBillingUseCase;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UseCaseConfigTest {

    @InjectMocks
    private UseCaseConfig useCaseConfig;

    @Mock
    private BillingGateway billingGateway;

    @Mock
    private LogisticGateway logisticGateway;

    @Test
    @DisplayName("Should return a MakeBillingUseCase instance")
    public void should_make_billing_use_case() {
        Assertions.assertThat(useCaseConfig.makeBillingUseCase(billingGateway))
                .isInstanceOf(MakeBillingUseCase.class);
    }

    @Test
    @DisplayName("Should return a CreateOrderUseCase instance")
    public void should_create_order_use_case() {
        Assertions.assertThat(useCaseConfig.createOrderUseCase(logisticGateway))
                .isInstanceOf(CreateOrderUseCase.class);
    }
}
