package com.hiberus.bill.config;

import com.hiberus.bill.usecase.BillingUseCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class UseCaseConfigTest {

    @InjectMocks
    private UseCaseConfig useCaseConfig;

    @Test
    public void should_return_BillingUseCase_instance() {
        assertThat(useCaseConfig.billingUseCase())
                .isInstanceOf(BillingUseCase.class);
    }
}
