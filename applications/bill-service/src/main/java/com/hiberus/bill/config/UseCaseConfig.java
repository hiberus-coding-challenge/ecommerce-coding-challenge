package com.hiberus.bill.config;

import com.hiberus.bill.usecase.BillingUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseCaseConfig {

    @Bean
    public BillingUseCase billingUseCase() {
        return new BillingUseCase();
    }
}
