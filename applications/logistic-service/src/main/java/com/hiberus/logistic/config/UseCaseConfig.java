package com.hiberus.logistic.config;

import com.hiberus.logistic.usecase.CreateOrderUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseCaseConfig {

    @Bean
    public CreateOrderUseCase createOrderUseCase() {
        return new CreateOrderUseCase();
    }
}
