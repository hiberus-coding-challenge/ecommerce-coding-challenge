# E-commerce coding challenge (Hiberus)

## E-commerce simulate

This project is a checkout simulate in an e-commerce. It is composed of three microservices.

1. Checkout service
    - [Checkout service will receive Orders in a REST Endpoint.](./applications/checkout-service)
2. Bill service
    - [Bill the sum of all products to user in Bill service.](./applications/bill-service)
3. Logistic service
    - [Logistic service to create a sent order.](./applications/logistic-service)

### Prerequisites to execute the project
You must have the following dependencies installed on your machine.

1. Docker version 19.03.9 or higher
2. Gradle version 6.4.1 or higher

### Run test
To run unit test of the project use the following command.

* ``> gradle test jacocoTestReport``

#### Run project with docker-compose
To execute this project you must follow the following steps.

1. Clean and build the project
    - ``> gradle clean build``

2. Run the project using docker-compose. This will create a docker container for each microservice and
run then on a specific network.
    - ``  > docker-compose up ``

The above command will execute the instructions given in the *docker-compose* file.

3. Test service in Swagger
    - [Go to Swagger](http://localhost:8080/swagger-ui.html)
    
### Technologies used
* Java version 11.0.7
* Spring boot version 2.3.1.RELEASE
* Spring Fox Swagger version 2.10.5
* Lombok version 1.18.12
* Junit version 5.6.2
* Mockito version 3.3.3

**Note: In case of not executing the project without docker, change the urls values in the application.yml
of the checkout microservice.**

**Note: didn't know much about docker and docker-compose, so check around a bit during the challenge.**